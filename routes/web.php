<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/geral', function () {
    return view('geral');
});

Route::get('/transaction-types', function () {
    return view('transaction-types');
});

Route::get('/clientes', function () {
    return view('clientes');
});

Route::get('/areadocliente', function () {
    return view('areadocliente');
});

Route::get('/transaction', function () {
    return view('transaction');
});

Route::get('/transaction-insert', function () {
    return view('transaction-insert');
});

Route::get('/admin', function () {
    return view('admin');
});

Route::get('/client-insert', function () {
    return view('client-insert');
});

Route::get('/type-insert', function () {
    return view('type-insert');
});