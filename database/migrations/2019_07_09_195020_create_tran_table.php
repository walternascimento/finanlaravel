<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTranTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('cliente_id')->references('id')->on('clientes')->onDelete('cascade');
            $table->integer('tipo_id')->references('id')->on('tipos')->onDelete('cascade');
            $table->string('data');
            $table->decimal('valor', 10, 2);
            $table->tinyInteger('credito');
            $table->timestamps();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trans');
    }
}
