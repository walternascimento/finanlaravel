$(document).ready(function () {
    request_get('api/tipos', makeTableType)
});

function makeTableType(data) {
    var table = document.getElementById("table_id").getElementsByTagName('tbody')[0];
    $.each(data.data, (i, item) => {
        var row = table.insertRow();
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        cell1.innerHTML = item.id;
        cell2.innerHTML = item.nome;
    });
    $('#table_id').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        }
    });
}

function request_get(url, action) {
    $.get(url)
        .done((msg) => { action(msg); })
        .fail((e) => { console.log(e); });
}