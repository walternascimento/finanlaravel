$(document).ready(function () {
    request_get('api/trans', makeTableType)
});

function makeTableType(data) {
    var table = document.getElementById("table_id").getElementsByTagName('tbody')[0];
    $.each(data.data, (i, item) => {
        var row = table.insertRow();
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);
        cell1.innerHTML = item.id;
        cell2.innerHTML = item.tipo;
        cell3.innerHTML = item.data;
        cell4.innerHTML = item.valor;
        cell5.innerHTML = (item.credito == 1) ? "Crédito" : "Débito";
    });
    $('#table_id').DataTable({
        "language": {
            "url": "https://cdn.datatables.net/plug-ins/1.10.19/i18n/Portuguese-Brasil.json"
        }
    });
}

function request_get(url, action) {
    $.get(url)
        .done((msg) => { action(msg); })
        .fail((e) => { console.log(e); });
}