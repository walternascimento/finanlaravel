$(document).ready(function () {
    request_get('api/clientes', makeSelectCli);
    request_get('api/tipos', makeSelectType);
    $( "form" ).on( "submit", function( event ) {
        console.log( $( this ).serialize());
        event.preventDefault();
        $.post( "api/trans",  $( this ).serialize() );
        $('#exampleModal').modal('show');
      });
});

function makeSelectType(data) {
    let select = $('select[name="tipo_id"]');
    select.append(`<option value="">Selecione um tipo</option>`);
    $.each(data.data, (i, item) => {
        select.append(`<option value="${item.id}">${item.nome}</option>`);
    });
}

function makeSelectCli(data) {
    let select = $('select[name="cliente_id"]');
    select.append(`<option value="">Selecione um cliente</option>`);
    $.each(data.data, (i, item) => {
        select.append(`<option value="${item.id}">${item.nome}</option>`);
    });
}

function request_get(url, action) {
    $.get(url)
        .done((msg) => { action(msg); })
        .fail((e) => { console.log(e); });
}