<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TranResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'tipo' => $this->tipo->nome,
            'tipo_id' => $this->tipo_id,
            'data' => $this->data,
            'valor' => $this->valor,
            'credito' => $this->credito,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
