<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\CommonController;
use App\Models\Tipo;

class TipoController extends CommonController
{
    public function __construct(Tipo $model)
    {
        $this->model = $model;
        $this->resource = "\App\Http\Resources\TipoResource";
    }
}