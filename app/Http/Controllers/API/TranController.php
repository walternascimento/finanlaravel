<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\CommonController;
use App\Models\Tran;

class TranController extends CommonController
{
    public function __construct(Tran $model)
    {
        $this->model = $model;
        $this->resource = "\App\Http\Resources\TranResource";
    }
}