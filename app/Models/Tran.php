<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tran extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['cliente_id', 'tipo_id', 'data', 'valor', 'credito'];

    public function tipo()
    {
        return $this->belongsTo(Tipo::class);
    }
}